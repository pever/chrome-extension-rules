chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('windows.html', {
    id: 'rulesId',
    bounds: {
      'width': 494,
      'height': 14
    },
    frame: {
      type: "none",
      activeColor: "none"
    },
    maxHeight: 14,
    alwaysOnTop: true
  });
});